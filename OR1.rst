.. Lecture documentation master file, created by
   sphinx-quickstart on Wed Dec  9 03:47:15 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

OR1
===================================

.. math::

   (a+b)^2 = a^2 + 2ab + b^2
   (a-b)^2 = a^2 - 2ab + b^2

.. math::
  
   (a+b)^2 &= (a+b)(a+b) \\
           &= a^2 + 2ab + b^2
